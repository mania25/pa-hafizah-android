package com.project_akhir.urbanfarming;

import android.app.ProgressDialog;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MainActivity extends AppCompatActivity {
    private VideoView videoStreamRaspi;
    private TextView lblSensorSuhu, lblSensorKelembaban, lblSensorIntensitasCahaya;
    private Button btnOpenWaterValve, btnCloseWaterValve;

    private String videoRTSPStreamURL = "http://172.20.10.2:8160";
    private final String mqttBrokerUrl = "tcp://137.59.126.83:1883";
    private MqttAndroidClient mqttAndroidClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        videoStreamRaspi = (VideoView)findViewById(R.id.videoStreamRaspi);

        lblSensorSuhu = (TextView)findViewById(R.id.lblSensorSuhu);
        lblSensorKelembaban = (TextView)findViewById(R.id.lblSensorKelembaban);
        lblSensorIntensitasCahaya = (TextView)findViewById(R.id.lblSensorIntensitasCahaya);

        btnOpenWaterValve = (Button)findViewById(R.id.btnOpenWaterValve);
        btnCloseWaterValve = (Button)findViewById(R.id.btnCloseWaterValve);

        videoStreamRaspi.setVideoURI(Uri.parse(videoRTSPStreamURL));
        videoStreamRaspi.setMediaController(new MediaController(MainActivity.this));
        videoStreamRaspi.requestFocus();
        videoStreamRaspi.start();

        final ProgressDialog dialog = ProgressDialog.show(MainActivity.this, "",
                "Loading. Please wait...", true);

        mqttAndroidClient = new MqttAndroidClient(getApplicationContext(), mqttBrokerUrl, "device-pa-hafizah-android");
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {
                if (reconnect){
                    Log.d("Reconnected to:", serverURI);
                } else {
                    Log.d("Connected To:", serverURI);

                    subscribeToTopic("/measurement_hafizah");
                }
            }

            @Override
            public void connectionLost(Throwable cause) {
                Log.d("Connected Lost:", cause.getMessage());

                cause.printStackTrace();
            }

            @Override
            public void messageArrived(final String topic, MqttMessage message) throws Exception {
                final String messageData = new String(message.getPayload());

                final String[] sensorData = messageData.split(":");

                System.out.println("Message: " + topic + " : " + messageData);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (topic.equals("/measurement_hafizah")) {
                            if (sensorData[0].equals("H")) {
                                lblSensorKelembaban.setText(sensorData[1] + " %");
                            } else if (sensorData[0].equals("T")) {
                                lblSensorSuhu.setText(sensorData[1] + " C");
                            } else if (sensorData[0].equals("LDR")) {
                                lblSensorIntensitasCahaya.setText(sensorData[1] + " Cd");
                            }
                        } else if (topic.equals("/relay_state_change_hafizah")){
                            if (messageData.equals("0")){
                                btnOpenWaterValve.setEnabled(true);
                                btnCloseWaterValve.setEnabled(false);
                            } else {
                                btnOpenWaterValve.setEnabled(false);
                                btnCloseWaterValve.setEnabled(true);
                            }
                        }
                    }
                });
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();

        mqttConnectOptions.setUserName("bbff39d0d3066758ffe55666762b3c8b150295b848cb6c871b79f2fff36c79fb");
        mqttConnectOptions.setPassword("50acea3098359517297e08040dc6bfc371d044190be6527c1ac29e078cbe8313".toCharArray());
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);

        try {
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();

                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);

                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);

                    subscribeToTopic("/measurement_hafizah");
                    subscribeToTopic("/relay_state_change_hafizah");

                    dialog.dismiss();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    exception.printStackTrace();

                    Log.d("Failed to connect to: ", mqttBrokerUrl);
                }
            });

            btnOpenWaterValve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    publishMessage("/controlling_hafizah", "1");
                }
            });

            btnCloseWaterValve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    publishMessage("/controlling_hafizah", "0");
                }
            });
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        videoStreamRaspi.start();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        videoStreamRaspi.start();
    }

    @Override
    protected void onPause() {
        super.onPause();

        videoStreamRaspi.stopPlayback();
    }

    @Override
    protected void onStop() {
        super.onStop();

        videoStreamRaspi.stopPlayback();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        videoStreamRaspi.stopPlayback();
    }


    public void subscribeToTopic(String mqttTopic){
        try {
            mqttAndroidClient.subscribe(mqttTopic, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.d("onSuccess: ", "SUBSCRIBED");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d("Failed to subscribe", exception.getMessage());
                }
            });
        } catch (Exception ex){
            System.err.println("Exception whilst subscribing");
            ex.printStackTrace();
        }
    }

    public void publishMessage(String mqttTopic, String command){

        try {
            MqttMessage message = new MqttMessage();
            message.setPayload(command.getBytes());
            mqttAndroidClient.publish(mqttTopic, message);
            Log.d("publishMessage: ", "Message Published");
            if(!mqttAndroidClient.isConnected()){
                Log.d("publishMessage: ", mqttAndroidClient.getBufferedMessageCount() + " messages in buffer.");;
            }
        } catch (Exception e) {
            System.err.println("Error Publishing: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
